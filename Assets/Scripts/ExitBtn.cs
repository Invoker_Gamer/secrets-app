﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitBtn : MonoBehaviour
{
    public AudioSource audio;
    private void OnMouseDown()
    {
        audio.Play();
        Application.Quit();
    }
}
