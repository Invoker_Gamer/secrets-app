﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AboutButton : MonoBehaviour
{
    public GameObject cam;
    private Animator animator;
    public AudioSource audio;
    public GameObject about;
    // Start is called before the first frame update
    void Start()
    {

        animator = cam.GetComponent<Animator>();
        
    }

    void OnMouseDown()
    {
        audio.Play();
        animator.SetTrigger("about");
        StartCoroutine(Example());  
    }

   IEnumerator Example()
    {
        yield return new WaitForSeconds(1.8f);
        about.SetActive(true);
    }
}
