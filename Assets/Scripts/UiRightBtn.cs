﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiRightBtn : MonoBehaviour
{
    public UIManager uimanager;
    public AudioSource audio;


    private void OnMouseDown()
    {
        audio.Play();
        uimanager.RightButton();
    }
}
