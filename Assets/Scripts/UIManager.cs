﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
   public List<GameObject> UIList = new List<GameObject>();
    public int listLength;
    private int activeIndex=0;
    private int nextIndex=0;
    private int prevIndex;
    void Start()
    {
        foreach (Transform child in transform)
        {
            UIList.Add(child.gameObject);
        }
        listLength = UIList.Count-1;
        prevIndex = listLength;
        activeIndex = 0;
        UIList[activeIndex].gameObject.SetActive(true);
        nextIndex = activeIndex+1;

    }

    public void RightButton()
    {
        print("clicked");
        prevIndex = activeIndex;
        UIList[activeIndex].gameObject.SetActive(false);
        UIList[nextIndex].gameObject.SetActive(true);
        activeIndex = nextIndex;
        nextIndex += 1;
        if (nextIndex > listLength)
        {
            nextIndex = 0;
        }
    }

    public void LeftButton()
    {
        print("Lclicked");
        UIList[activeIndex].gameObject.SetActive(false);
        UIList[prevIndex].gameObject.SetActive(true);
        activeIndex = prevIndex;
        prevIndex -= 1;

        if (prevIndex < 0)
        {
            prevIndex = listLength;
        }

    }
}
