﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProbLeftBtn : MonoBehaviour
{
    public ProbManager probManager;
    public AudioSource audio;
    private void OnMouseDown()
    {
        audio.Play();
        probManager.LeftButton();
    }
}
