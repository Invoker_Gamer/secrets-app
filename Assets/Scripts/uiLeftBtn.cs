﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class uiLeftBtn : MonoBehaviour
{
    public UIManager uimanager;
    public AudioSource audio;

    private void OnMouseDown()
    {
        audio.Play();
        uimanager.LeftButton();
    }
}
