﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProbsButton : MonoBehaviour
{
    public GameObject cam;
    private Animator animator;
    public AudioSource audio;
    // Start is called before the first frame update
    void Start()
    {
        animator = cam.GetComponent<Animator>();
    }

    void OnMouseDown()
    {
        audio.Play();
        animator.SetTrigger("prob");

    }
}
