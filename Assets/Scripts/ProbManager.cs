﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProbManager : MonoBehaviour
{
    public List<GameObject> probsList = new List<GameObject>();
    public int listLength;
    private int activeIndex=0;
    private int nextIndex=0;
    private int prevIndex;
    void Start()
    {
        foreach (Transform child in transform)
        {
            probsList.Add(child.gameObject);
        }
        listLength = probsList.Count-1;
        prevIndex = listLength;
        activeIndex = 0;
        probsList[activeIndex].gameObject.SetActive(true);
        nextIndex = activeIndex+1;

    }

    public void RightButton()
    {
        print("clicked");
        prevIndex = activeIndex;
        probsList[activeIndex].gameObject.SetActive(false);
        probsList[nextIndex].gameObject.SetActive(true);
        activeIndex = nextIndex;
        nextIndex+=1;
        if (nextIndex >  listLength)
        {
            nextIndex = 0;
        }
    }

    public void LeftButton()
    {
        print("Lclicked");
        probsList[activeIndex].gameObject.SetActive(false);
        probsList[prevIndex].gameObject.SetActive(true);
        activeIndex = prevIndex;
        prevIndex -= 1;
        
        if (prevIndex < 0)
        {
            prevIndex = listLength;
        }

    }
}
