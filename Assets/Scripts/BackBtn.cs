﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackBtn : MonoBehaviour
{
    public GameObject cam;
    private Animator animator;
    public AudioSource audio;

    void Start()
    {
        animator = cam.GetComponent<Animator>();
    }

    void OnMouseDown()
    {
        audio.Play();
        if (this.gameObject.tag == "ui")
        {
            animator.SetTrigger("ui_out");
        }
        if (this.gameObject.tag == "about")
        {
            animator.SetTrigger("about_out");
        }
        if (this.gameObject.tag == "prob")
        {
            animator.SetTrigger("prob_out");
        }
    }

}
